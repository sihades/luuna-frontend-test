# Luuna Frontend Test - Github API

This a frontend test assignment for Luuna ZeBrands.

## Setup

To set up this project first we need to install the needed libraries. Please run next command before start.

### `npm install`

After install libraries please configure environment variables,
so just copy the .env-sample to .env and modify the content with correct variables.

### `cp .env-sample .env`

## Testing

For this project we use the standard testing library.

- To run only the tests execute:

### `npm run test`

- To run tests with coverage report execute:

### `npm run coverage`

## Starting UI server

In the project directory, you must run:

### `npm run start`

Now open http://localhost:3000 to play with the UI :).
