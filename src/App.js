import React from 'react';
import {
  BrowserRouter,
  Redirect,
  Route,
  Switch,
} from 'react-router-dom';
import ScreenWrapper from './screens/Root';
import ScreenUser from './screens/Users';
import ScreenRepository from './screens/Repository';

const App = () => (
  <BrowserRouter>
    <Switch>
      <Route
        path="/users"
        render={(props) => <ScreenWrapper props={props} screen={ScreenUser} />}
      />
      <Route
        path="/repositories"
        render={(props) => <ScreenWrapper props={props} screen={ScreenRepository} />}
      />
      <Route path="/">
        <Redirect to={{ pathname: '/users' }} />
      </Route>
    </Switch>
  </BrowserRouter>
);

export default App;
