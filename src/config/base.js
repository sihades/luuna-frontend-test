export default {
  API_URL: process.env.REACT_APP_GITHUB_BASE_URL,
  ENDPOINTS: {
    USERS: '/search/users',
    REPOSITORIES: '/search/repositories',
  },
};
