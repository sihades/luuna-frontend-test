import { createTheme } from '@material-ui/core/styles';
import { common } from '@material-ui/core/colors';

export const colors = {
  background: '#F4F8FA',
  black: common.black,
  blueDark: '#298FD6',
  blueLight: '#7BD2F6',
  highlight: '#E0F3FD',
  white: common.white,
};

export const drawerWidth = 248;

export default createTheme({
  palette: {
    background: {
      default: colors.white,
    },
    primary: {
      light: colors.blueLight,
      main: colors.blueLight,
      dark: colors.blueLight,
      contrastText: colors.blueDark,
    },
    secondary: {
      light: colors.white,
      main: colors.white,
      dark: colors.white,
      contrastText: colors.blueDark,
    },
  },
});
