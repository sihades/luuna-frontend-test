import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Box } from '@material-ui/core';
import Pagination from '@material-ui/lab/Pagination';
import base from '../../config/base';
import SearchBar from '../SearchBar';
import styles from './styles';

const headers = {
  Accept: 'application/vnd.github.v3+json',
};

const Search = ({ endpoint, wrapper: Wrapper }) => {
  const classes = styles();
  const itemsPerPage = 10;
  const defaultPage = 1;
  const defaultTotalPages = 1;
  const [query, setQuery] = useState('');
  const [currentQuery, setCurrentQuery] = useState('');
  const [currentPage, setCurrentPage] = useState(defaultPage);
  const [totalPages, setTotalPages] = useState(defaultTotalPages);
  const [data, setData] = useState([]);
  const [onSearch, setOnSearch] = useState(false);

  const requestSearch = () => {
    const page = (query !== currentQuery) ? defaultPage : currentPage;
    axios.get(`${base.API_URL}${endpoint}`, {
      headers,
      params: {
        q: query,
        per_page: itemsPerPage,
        page,
      },
    })
      .then((response) => {
        setData(response.data.items);
        setCurrentQuery(query);
        setCurrentPage(page);
        setTotalPages(Math.ceil(response.data.total_count / itemsPerPage));
        setOnSearch(false);
      })
      .catch(() => setData([]));
  };

  const onPaginate = (e, page) => {
    e.preventDefault();
    setCurrentPage(page);
    setOnSearch(true);
  };

  useEffect(() => onSearch && requestSearch(), [onSearch]);

  return (
    <Box>
      <SearchBar q={query} onChange={setQuery} onSearch={() => setOnSearch(true)} />
      <Wrapper data={data} />
      <Pagination
        className={classes.pagination}
        color="primary"
        count={totalPages}
        page={currentPage}
        onChange={onPaginate}
      />
    </Box>
  );
};

export default Search;
