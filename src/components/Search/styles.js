import { makeStyles } from '@material-ui/core/styles';

const styles = makeStyles({
  pagination: {
    marginTop: '16px',
  },
});

export default styles;
