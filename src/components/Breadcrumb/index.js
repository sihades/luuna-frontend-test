import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { Breadcrumbs, Link } from '@material-ui/core';
import styles from './styles';

const LinkRouter = (props) => <Link {...props} component={RouterLink} />;

const breadcrumbNameMap = {
  '/users': 'Users',
  '/repositories': 'Repositories',
};

const Breadcrumb = ({ pathNames }) => {
  const classes = styles();

  const renderBreadcrumbItems = () => pathNames.map((value, index) => {
    const to = `/${pathNames.slice(0, index + 1).join('/')}`;
    return (
      <LinkRouter color="inherit" to={to} key={to} classes={{ root: classes.breadcrumbItem }}>
        {`:: ${breadcrumbNameMap[to]}`}
      </LinkRouter>
    );
  });

  return (
    <Breadcrumbs aria-label="breadcrumb" classes={{ root: classes.breadcrumb }}>
      {renderBreadcrumbItems()}
    </Breadcrumbs>
  );
};

export default Breadcrumb;
