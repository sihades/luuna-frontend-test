import { makeStyles } from '@material-ui/core/styles';
import { colors } from '../../config/theme';

const styles = makeStyles({
  breadcrumb: {
    marginBottom: '32px',
  },
  breadcrumbItem: {
    textTransform: 'lowercase',
    fontSize: '14px',
    color: colors.blueLight,
  },
});

export default styles;
