import React from 'react';
import { MemoryRouter } from 'react-router';
import { cleanup, render } from '@testing-library/react';
import renderer from 'react-test-renderer';
import ComponentBreadcrumb from './index';

let component;

const pathNames = [
  'users',
];

beforeEach(() => {
  cleanup();
  component = (
    <MemoryRouter>
      <ComponentBreadcrumb pathNames={pathNames} />
    </MemoryRouter>
  );
});

test('Rendering ComponentBreadcrumb correctly.', () => {
  const tree = renderer.create(component).toJSON();
  expect(tree).toMatchSnapshot();

  const { getByText } = render(component);
  expect(getByText(':: Users')).toBeInTheDocument();
});
