import React from 'react';
import { withRouter } from 'react-router-dom';
import { List, ListItem, ListItemIcon } from '@material-ui/core';
import PeopleIcon from '@material-ui/icons/People';
import StorageIcon from '@material-ui/icons/Storage';
import styles from './styles';

const Items = ({ location, history }) => {
  const classes = styles();

  const redirect = (path) => {
    history.push(path);
  };

  const renderItem = (path, title, Icon) => {
    const urlPath = location.pathname;
    const isActive = (path === '/' && urlPath === path)
      || (path !== '/' && urlPath.includes(path));

    return (
      <ListItem
        classes={{ root: (isActive) ? classes.listItemActive : classes.listItem }}
        button
        onClick={() => redirect(path)}
      >
        <ListItemIcon className={classes.listItemIcon}>
          <Icon className={classes.icon} />
        </ListItemIcon>
        {title}
      </ListItem>
    );
  };

  return (
    <List>
      { renderItem('/users', 'Users', PeopleIcon) }
      { renderItem('/repositories', 'Repositories', StorageIcon) }
    </List>
  );
};

export default withRouter(Items);
