import { makeStyles } from '@material-ui/core/styles';
import { colors, drawerWidth } from '../../config/theme';

const styles = makeStyles((props) => (
  {
    wrapper: {
      display: 'flex',
      position: 'relative',
    },
    drawer: {
      [props.breakpoints.up('sm')]: {
        height: '100%',
        width: `${drawerWidth}px`,
        position: 'relative',
        paddingLeft: '32px',
        paddingTop: '16px',
        background: colors.background,
        color: colors.blueDark,
      },
    },
    appBar: {
      width: '100%',
      marginLeft: '0px',
      position: 'relative',
      [props.breakpoints.up('sm')]: {
        display: 'none',
        height: '0px',
      },
    },
    menuButton: {
      marginRight: props.spacing(2),
      [props.breakpoints.up('sm')]: {
        display: 'none',
      },
    },
    toolbar: props.mixins.toolbar,
    drawerPaper: {
      width: drawerWidth,
    },
    logo: {
      backgroundRepeat: 'no-repeat',
      display: 'block',
      height: '64px',
      margin: '8px',
      width: '64px',
    },
    listItem: {
      fontWeight: 'normal',
    },
    listItemActive: {
      background: colors.highlight,
      fontWeight: 'bold',
    },
    listItemIcon: {
      minWidth: '32px',
    },
    icon: {
      fill: colors.blueDark,
    },
  }
));

export default styles;
