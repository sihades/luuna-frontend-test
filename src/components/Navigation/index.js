import React, { useState } from 'react';
import {
  AppBar,
  Drawer,
  Hidden,
  IconButton,
  Link,
  Toolbar,
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import { useTheme } from '@material-ui/core/styles';
import Items from './Items';
import styles from './styles';
import logo from '../../assets/images/logo.png';

const Navigation = (props) => {
  const theme = useTheme();
  const classes = styles();
  const [mobileOpen, setMobileOpen] = useState(false);

  const handleDrawerToggle = (e) => {
    e.preventDefault();
    setMobileOpen(!mobileOpen);
  };

  const renderLogo = () => (
    <Link href="/">
      <img className={classes.logo} src={logo} alt="Luuna ZeBrands" />
    </Link>
  );

  const renderItems = () => <Items props={props} />;

  return (
    <div className={classes.wrapper}>
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open menu"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          {renderLogo()}
        </Toolbar>
      </AppBar>
      <Hidden smUp implementation="css">
        <Drawer
          className={classes.drawer}
          variant="temporary"
          anchor={theme.direction === 'rtl' ? 'right' : 'left'}
          open={mobileOpen}
          onClose={handleDrawerToggle}
          classes={{
            paper: classes.drawerPaper,
          }}
          ModalProps={{
            keepMounted: true,
          }}
        >
          {renderItems()}
        </Drawer>
      </Hidden>
      <Hidden xsDown implementation="css">
        <Drawer
          className={classes.drawer}
          variant="permanent"
          anchor="left"
          open
          classes={{
            paper: classes.drawerPaper,
          }}
        >
          {renderLogo()}
          {renderItems()}
        </Drawer>
      </Hidden>
    </div>
  );
};

export default Navigation;
