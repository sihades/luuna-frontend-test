import { makeStyles } from '@material-ui/core/styles';

const styles = makeStyles((props) => (
  {
    form: {
      display: 'flex',
      marginBottom: '16px',
      width: '50%',
      [props.breakpoints.down('sm')]: {
        width: '100%',
      },
    },
    input: {
      padding: '8px',
      width: '100%',
    },
  }
));

export default styles;
