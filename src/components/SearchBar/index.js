import React from 'react';
import {
  IconButton,
  InputBase,
  Paper,
} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import styles from './styles';

const SearchBar = ({ q, onChange, onSearch }) => {
  const classes = styles();

  return (
    <Paper
      className={classes.form}
      component="form"
      onSubmit={(e) => e.preventDefault()}
    >
      <InputBase
        placeholder="Search"
        className={classes.input}
        value={q}
        onChange={(e) => onChange(e.target.value)}
        inputProps={{ 'aria-label': 'search github api' }}
      />
      <IconButton type="submit" aria-label="search" onClick={onSearch}>
        <SearchIcon />
      </IconButton>
    </Paper>
  );
};

export default SearchBar;
