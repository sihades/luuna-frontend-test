import { makeStyles } from '@material-ui/core/styles';
import { colors, drawerWidth } from '../config/theme';

const styles = makeStyles((props) => (
  {
    container: {
      background: colors.white,
      height: '100%',
      margin: '0',
      padding: '0',
      width: '100%',
    },
    layout: {
      marginLeft: `${drawerWidth}px`,
      padding: '8px 32px',
      boxShadow: 'none',
      [props.breakpoints.down('sm')]: {
        marginLeft: '0px',
      },
    },
  }
));

export default styles;
