import { makeStyles } from '@material-ui/core/styles';

const styles = makeStyles({
  chip: {
    display: 'relative',
    position: 'absolute',
    right: '32px',
  },
});

export default styles;
