import React from 'react';
import {
  Avatar,
  Chip,
  List as MList,
  ListItem,
  ListItemAvatar,
  ListItemText, Typography,
} from '@material-ui/core';
import styles from './styles';

const List = ({ data }) => {
  const classes = styles();

  return (
    <MList component="nav" aria-label="github repository list">
      {data.map((item) => (
        <ListItem key={item.id} button component="a" href={item.html_url} target="_blank">
          <ListItemAvatar>
            <Avatar alt={item.owner.login} src={item.owner.avatar_url} />
          </ListItemAvatar>
          <ListItemText
            primary={(
              <>
                <Typography component="span">{item.name}</Typography>
                {(item.language) && <Chip className={classes.chip} label={item.language} color="primary" />}
              </>
            )}
            secondary={item.owner.login}
          />
        </ListItem>
      ))}
    </MList>
  );
};

export default List;
