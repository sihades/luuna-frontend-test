import React from 'react';
import Search from '../../components/Search';
import base from '../../config/base';
import ScreenRepositoryList from './List';

export default () => (
  <Search endpoint={base.ENDPOINTS.REPOSITORIES} wrapper={ScreenRepositoryList} />
);
