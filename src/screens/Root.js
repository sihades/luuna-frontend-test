import React from 'react';
import { CssBaseline, Paper } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/core/styles';
import ComponentNavigation from '../components/Navigation';
import ComponentBreadcrumb from '../components/Breadcrumb';
import theme from '../config/theme';
import styles from './styles';

const Root = ({ screen: Screen, props }) => {
  const classes = styles();
  const pathNames = props.location.pathname.split('/').filter((x) => x);

  return (
    <>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <main className={classes.container}>
          <ComponentNavigation props={props} />
          <Paper classes={{ root: classes.layout }}>
            <ComponentBreadcrumb pathNames={pathNames} />
            <Screen />
          </Paper>
        </main>
      </ThemeProvider>
    </>
  );
};

export default Root;
