import React from 'react';
import {
  Avatar,
  List as MList,
  ListItem,
  ListItemAvatar,
  ListItemText,
} from '@material-ui/core';

const List = ({ data }) => (
  <MList component="nav" aria-label="github users list">
    {data.map((item) => (
      <ListItem key={item.id} button component="a" href={item.html_url} target="_blank">
        <ListItemAvatar>
          <Avatar alt={item.login} src={item.avatar_url} />
        </ListItemAvatar>
        <ListItemText primary={item.login} />
      </ListItem>
    ))}
  </MList>
);

export default List;
