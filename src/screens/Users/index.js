import React from 'react';
import Search from '../../components/Search';
import base from '../../config/base';
import ScreenUserList from './List';

export default () => <Search endpoint={base.ENDPOINTS.USERS} wrapper={ScreenUserList} />;
